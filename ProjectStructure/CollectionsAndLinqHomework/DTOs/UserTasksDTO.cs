﻿using CollectionsAndLinqHomework.Models.User;
using System.Collections.Generic;

namespace CollectionsAndLinqHomework.DTOs
{
    class UserTasksDTO
    {
        public User User { get; set; }
        public List<TaskDTO> Tasks { get; set; }
    }
}
