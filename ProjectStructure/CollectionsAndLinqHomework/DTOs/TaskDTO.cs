﻿using CollectionsAndLinqHomework.Models;
using CollectionsAndLinqHomework.Models.User;
using System;

namespace CollectionsAndLinqHomework.DTOs
{
    class TaskDTO
    {
        public int Id { get; set; }
        public User Performer { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }

        public override string ToString()
        {
            return $"\"{Name}\" created at {CreatedAt} now is {State}";
        }
    }
}
