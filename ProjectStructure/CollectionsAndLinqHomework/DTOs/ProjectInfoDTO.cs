﻿namespace CollectionsAndLinqHomework.DTOs
{
    class ProjectInfoDTO
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO LongestTaskByDescription { get; set; }
        public TaskDTO ShortestTaskByName { get; set; }
        public int UsersNumber { get; set; }

        public override string ToString()
        {
            return $"Project: {Project}\r\n\r\nLongest task by descrition:\r\n\t{LongestTaskByDescription}\r\n" +
                $"Shortest task by name:\r\n\t{ShortestTaskByName}\r\nUsers quantity: {UsersNumber}\r\n\r\n";
        }
    }
}
