﻿using CollectionsAndLinqHomework.Models.Team;
using CollectionsAndLinqHomework.Models.User;
using System;
using System.Collections.Generic;

namespace CollectionsAndLinqHomework.DTOs
{
    class ProjectDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
        public List<TaskDTO> Tasks { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }

        public override string ToString()
        {
            return $"\"{Name}\" created at {CreatedAt}  by {Author.FirstName} {Author.LastName}";
        }
    }
}
