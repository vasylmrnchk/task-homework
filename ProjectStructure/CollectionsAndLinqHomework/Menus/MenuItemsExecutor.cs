﻿using System;
using System.Threading.Tasks;

namespace CollectionsAndLinqHomework.Menus
{
    internal static class MenuItemsExecutor
    {
        public static ClientService ModelService { get; set; }

        public static async Task<string> Create<T, TNew>(TNew item)
        {
            try
            {
                var created = await ModelService.CreateAsync<T, TNew>(item);
                return $"{typeof(T).Name} \"{created}\" created successfully.";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static async Task<string> Get<T>(int id)
        {
            try
            {
                var entity = await ModelService.GetAsync<T>(id);
                return entity.ToString();
            }
            catch (Exception e) { return e.Message; }

        }

        public static async Task<string> Update<T, TUpdate>(TUpdate item)
        {
            try
            {
                await ModelService.UpdateAsync<TUpdate>(item);
                return $"{typeof(T).Name} \"{item}\" updated successfully.";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static async Task<string> Delete<T>(int id)
        {
            try
            {
                await ModelService.RemoveAsync(id);
                return $"{typeof(T).Name} with id={id} removed successfully.";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static async Task<string> GetAll<T>()
        {
            try
            {
                var entities = await ModelService.GetAllAsync<T>();

                return MenuLinqItemsExecutor.PrintList<T>(entities, $"List of { typeof(T).Name}s", $"{typeof(T).Name}");
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}

