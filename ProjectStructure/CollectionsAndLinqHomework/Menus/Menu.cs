﻿using CollectionsAndLinqHomework.Models.Project;
using CollectionsAndLinqHomework.Models.Task;
using CollectionsAndLinqHomework.Models.Team;
using CollectionsAndLinqHomework.Models.User;
using System;
using static CollectionsAndLinqHomework.Menus.MenuItemsExecutor;
using static CollectionsAndLinqHomework.Menus.MenuLinqItemsExecutor;
using static CollectionsAndLinqHomework.ViewCreator;
using Tasks = System.Threading.Tasks;

namespace CollectionsAndLinqHomework.Menus
{
    internal static class Menu
    {
        readonly static int delay = 1000;
        readonly static string subMenuFooterText = "Press <Enter> for come back to last menu: ";
        readonly static string menuFooterText = "Select an option: ";
        readonly static string[] mainMenuNames = {
            "1) Quantity of user task(s) in project(s)",
            "2) User task(s) with  task name shorter 45 chars",
            "3) User task(s) finished in 2021 year",
            "4) User(s) in team(s) which are older than 10 year old",
            "5) User(s) sorted by name with task(s) sorted by task name length",
            "6) User info",
            "7) Projects info",
            $"8) Complete random Task after {delay} ms delay",
            "9) Exit",
            "===================================",
            "a) Project menu",
            "b) Task menu",
            "c) Team menu",
            "d) User menu"
        };

        readonly static string[] subMenuNames = {
            "1) Create",
            "2) Update",
            "3) Delete",
            "4) Print all",
            "5) Back to main menu"
        };

        internal static async Tasks.Task<bool> MainMenu()
        {

            string option = CreateMenuItemView(
                "Main menu", String.Join("\n", mainMenuNames), menuFooterText);

            await LinqExecutor.BasicCollections.GetAllData();
            LinqExecutor.CreateProjectList();
            try
            {
                int id;
                switch (option)
                {
                    case "1":
                        id = CreateIdForm("user");
                        CreateMenuItemView($"User with id ={id} has: ", MenuItem1(id), subMenuFooterText);
                        return true;
                    case "2":
                        id = CreateIdForm("user");
                        CreateMenuItemView("", MenuItem2(id), subMenuFooterText);
                        return true;
                    case "3":
                        id = CreateIdForm("user");
                        CreateMenuItemView("", MenuItem3(id), subMenuFooterText);
                        return true;
                    case "4":
                        CreateMenuItemView($"Users in Teams which are older than 10 year old:", MenuItem4(), subMenuFooterText);
                        return true;
                    case "5":
                        CreateMenuItemView($"Users sorted by name with tasks sorted by task name length descending:", MenuItem5(), subMenuFooterText);
                        return true;
                    case "6":
                        id = CreateIdForm("user");
                        CreateMenuItemView($"Info of user with id={id}:", MenuItem6(id), subMenuFooterText);
                        return true;
                    case "7":
                        CreateMenuItemView($"Projects info:", MenuItem7(), subMenuFooterText);
                        return true;
                    case "8":
                        await MenuItem8(delay);
                        CreateMenuItemView($"Task log:", PrintList<string>(TasksLog, "", ""), subMenuFooterText);
                        return true;
                    case "9":
                        return false;
                    case "a":
                        while (await ProjectMenu())
                        {
                        }
                        return true;
                    case "b":
                        while (await TaskMenu())
                        {
                        }
                        return true;
                    case "c":
                        while (await TeamMenu())
                        {
                        }
                        return true;
                    case "d":
                        while (await UserMenu())
                        {
                        }
                        return true;
                    default:
                        return true;
                }
            }
            catch
            {
                return true;
            }
        }

        private static async Tasks.Task<bool> UserMenu()
        {
            string option = CreateMenuItemView($"User menu", String.Join("\n", subMenuNames),
                              menuFooterText);

            ModelService.Endpoint = "users";
            switch (option)
            {
                case "1":
                    CreateMenuItemView("", await Create<User, NewUser>(CreateUserForm()), subMenuFooterText);
                    return true;
                case "2":
                    CreateMenuItemView("", await Update<User, UpdateUser>(CreateUpdateUserForm()), subMenuFooterText);
                    return true;
                case "3":
                    CreateMenuItemView("", await Delete<User>(CreateIdForm("user")), subMenuFooterText);
                    return true;
                case "4":
                    CreateMenuItemView("", await GetAll<User>(), subMenuFooterText);
                    return true;
                case "5":
                    return false;
                default:
                    return true;
            }
        }

        private static async Tasks.Task<bool> TeamMenu()
        {
            string option = CreateMenuItemView($"Team menu", String.Join("\n", subMenuNames),
                              menuFooterText);

            ModelService.Endpoint = "teams";
            switch (option)
            {
                case "1":
                    CreateMenuItemView("", await Create<Team, NewTeam>(CreateTeamForm()), subMenuFooterText);
                    return true;
                case "2":
                    CreateMenuItemView("", await Update<Team, UpdateTeam>(CreateUpdateTeamForm()), subMenuFooterText);
                    return true;
                case "3":
                    CreateMenuItemView("", await Delete<Team>(CreateIdForm("team")), subMenuFooterText);
                    return true;
                case "4":
                    CreateMenuItemView("", await GetAll<Team>(), subMenuFooterText);
                    return true;
                case "5":
                    return false;
                default:
                    return true;
            }
        }

        private static async Tasks.Task<bool> TaskMenu()
        {
            string option = CreateMenuItemView($"Task menu", String.Join("\n", subMenuNames),
                              menuFooterText);

            ModelService.Endpoint = "tasks";
            switch (option)
            {
                case "1":
                    CreateMenuItemView("", await Create<Task, NewTask>(CreateTaskForm()), subMenuFooterText);
                    return true;
                case "2":
                    CreateMenuItemView("", await Update<Task, UpdateTask>(CreateUpdateTaskForm()), subMenuFooterText);
                    return true;
                case "3":
                    CreateMenuItemView("", await Delete<Task>(CreateIdForm("task")), subMenuFooterText);
                    return true;
                case "4":
                    CreateMenuItemView("", await GetAll<Task>(), subMenuFooterText);
                    return true;
                case "5":
                    return false;
                default:
                    return true;
            }
        }

        private static async Tasks.Task<bool> ProjectMenu()
        {
            string option = CreateMenuItemView($"Project menu", String.Join("\n", subMenuNames),
                              menuFooterText);

            ModelService.Endpoint = "projects";
            switch (option)
            {
                case "1":
                    CreateMenuItemView("", await Create<Project, NewProject>(CreateProjectForm()), subMenuFooterText);
                    return true;
                case "2":
                    CreateMenuItemView("", await Update<Project, UpdateProject>(CreateUpdateProjectForm()), subMenuFooterText);
                    return true;
                case "3":
                    CreateMenuItemView("", await Delete<Project>(CreateIdForm("project")), subMenuFooterText);
                    return true;
                case "4":
                    CreateMenuItemView("", await GetAll<Project>(), subMenuFooterText);
                    return true;
                case "5":
                    return false;
                default:
                    return true;
            }
        }
    }
}
