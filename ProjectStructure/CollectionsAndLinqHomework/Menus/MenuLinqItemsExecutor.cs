﻿using CollectionsAndLinqHomework.DTOs;
using CollectionsAndLinqHomework.Models.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsAndLinqHomework.Menus
{
    internal static class MenuLinqItemsExecutor
    {
        public static QueryExecutor LinqExecutor { get; set; }
        public static List<string> TasksLog { get; private set; } = new();
        public static string MenuItem1(int id)
        {
            var projectDictionary = LinqExecutor.GetUserTasksQuantityPerProject(id);
            return PrintDictionary<ProjectDTO, int>(projectDictionary, "task(s) in project");
        }
        public static string MenuItem2(int id)
        {
            var taskList = LinqExecutor.GetUserTasksWithTaskNameLongThan45(id);
            return PrintList<TaskDTO>(taskList, $"Tasks with name shorter than 45 chars of User with id={id} ", "Task");
        }
        public static string MenuItem3(int id)
        {
            var finishedTasks = LinqExecutor.GetUserTasksFinishedIn2021(id);
            return PrintList<LiteTaskDTO>(finishedTasks, $"Task(s) finished in 2021 year of user with id={id}:", "Task");
        }
        public static string MenuItem4()
        {
            var teams = LinqExecutor.GetTeamsWithUsersOlderThan10Years();
            return PrintTeamsList(teams, "Team");
        }
        public static string MenuItem5()
        {
            var sortedUsers = LinqExecutor.GetSortedByNameAndTaskNameLengthUsers();
            return PrintSortedUsers(sortedUsers, "User");
        }
        public static string MenuItem6(int id)
        {
            var userInfo = LinqExecutor.GetUserInfo(id);
            if (userInfo is null)
            {
                return "User info not found";
            }
            return userInfo.ToString();
        }
        public static string MenuItem7()
        {
            var projectsInfo = LinqExecutor.GetProjectsInfo();
            return PrintList<ProjectInfoDTO>(projectsInfo, "", "");
        }
        public static async Task MenuItem8(int delay)
        {
            try
            {
                var markedTaskId = await LinqExecutor.MarkRandomTaskWithDelay(delay);
                TasksLog.Add($"{DateTime.Now} - Task with id={markedTaskId} marked as Complited");
            }
            catch (System.Exception e)
            {
                TasksLog.Add($"{ DateTime.Now} - {e.Message}");
            }
        }
        static string PrintTeamsList(List<TeamDTO> teams, string message)
        {
            var builder = new StringBuilder();
            foreach (var item in teams)
            {
                builder.AppendLine(PrintList<User>(item.Users, $"{message}: {item.Name.ToUpper()}", "User:"));
            }

            return builder.ToString();
        }
        static string PrintSortedUsers(List<UserTasksDTO> users, string message)
        {
            var builder = new StringBuilder();
            foreach (var item in users)
            {
                builder.AppendLine(PrintList<TaskDTO>(item.Tasks, $"{message}: {item.User.ToString().ToUpper()}", "Task:"));
            }
            return builder.ToString();
        }
        static string PrintDictionary<TKey, TValue>(Dictionary<TKey, TValue> dictionary, string message)
        {
            var builder = new StringBuilder();
            foreach (var item in dictionary)
            {
                builder.AppendLine($"\t{item.Value} {message} {item.Key}");
            }
            return builder.ToString();
        }
        public static string PrintList<T>(List<T> list, string header, string message)
        {
            var builder = new StringBuilder();
            builder.AppendLine(header);
            foreach (var item in list)
            {
                builder.AppendLine($"\t{message} {item}");
            }

            return builder.ToString();
        }
    }
}

