﻿using CollectionsAndLinqHomework.Models;
using CollectionsAndLinqHomework.Models.Project;
using CollectionsAndLinqHomework.Models.Task;
using CollectionsAndLinqHomework.Models.Team;
using CollectionsAndLinqHomework.Models.User;
using System.Collections.Generic;
using Tasks = System.Threading.Tasks;

namespace CollectionsAndLinqHomework
{
    class BasicCollections
    {
        private readonly ClientService service = new();
        public List<Project> Projects { get; private set; } = new List<Project>();
        public List<Task> Tasks { get; private set; } = new List<Task>();
        public List<Team> Teams { get; private set; } = new List<Team>();
        public List<User> Users { get; private set; } = new List<User>();

        public BasicCollections()
        {
        }

        public async Tasks.Task GetAllData()
        {
            service.Endpoint = "projects";
            Projects = await service.GetAllAsync<Project>();

            await GetTasks();

            service.Endpoint = "teams";
            Teams = await service.GetAllAsync<Team>();

            service.Endpoint = "users";
            Users = await service.GetAllAsync<User>();
        }

        public async Tasks.Task GetTasks()
        {
            service.Endpoint = "tasks";
            Tasks = await service.GetAllAsync<Task>();
        }

        public async Tasks.Task<int> CompleteTask(Task task)
        {
            var completed = new UpdateTask
            {
                Id = task.Id,
                Description = task.Description,
                Name = task.Name,
                FinishedAt = System.DateTime.Now,
                State = TaskState.Completed,
            };

            service.Endpoint = "tasks";
            await service.UpdateAsync<UpdateTask>(completed);
            return completed.Id;
        }
    }
}
