﻿namespace CollectionsAndLinqHomework.Models
{
    public enum TaskState
    {
        Created,
        Started,
        Completed,
        Canceled
    }
}
