﻿using System;

namespace CollectionsAndLinqHomework.Models.Team
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public override string ToString()
        {
            return $"{Name} created at: {CreatedAt}";
        }
    }
}
