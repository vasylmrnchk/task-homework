﻿using System;

namespace CollectionsAndLinqHomework.Models.Task
{
    class Task
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
        public override string ToString()
        {
            return $"\"{Name}\" created at {CreatedAt} now is {State}" +
                $"\r\n\t\t{Description}\r\n"; ;
        }
    }
}
