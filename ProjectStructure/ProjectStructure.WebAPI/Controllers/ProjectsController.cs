﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO.Project;
using ProjectStructure.BLL.Services.Abstract;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class ProjectsController : ControllerBase
    {
        readonly IProjectsService _service;
        public ProjectsController(IProjectsService service)
        {
            _service = service;
        }

        // GET: api/<ProjectsController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectDTO>>> Get()
        {
            return Ok(await _service.ReadAllAsync());
        }

        // GET api/<ProjectsController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> Get(int id)
        {
            return Ok(await _service.ReadAsync(id));
        }

        // POST api/<ProjectsController>
        [HttpPost]
        public async Task<ActionResult<ProjectDTO>> Post([FromBody] NewProjectDTO newProjectDTO)
        {
            var project = await _service.CreateAsync(newProjectDTO);
            return CreatedAtAction(nameof(Get), new { id = project.Id }, project);
        }

        // PUT api/<ProjectsController>
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] UpdateProjectDTO updateProjectDTO)
        {
            await _service.UpdateAsync(updateProjectDTO);
            return Ok();
        }

        // DELETE api/<ProjectsController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            await _service.DeleteAsync(id);
            return NoContent();
        }
    }
}
