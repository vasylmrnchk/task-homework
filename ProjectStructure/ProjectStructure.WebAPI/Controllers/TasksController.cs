﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO.Task;
using ProjectStructure.BLL.Services.Abstract;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class TasksController : ControllerBase
    {
        readonly ITasksService _service;
        public TasksController(ITasksService service)
        {
            _service = service;
        }

        // GET: api/<TasksController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> Get()
        {
            return Ok(await _service.ReadAllAsync());
        }

        // GET api/<TasksController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TaskDTO>> Get(int id)
        {
            return Ok(await _service.ReadAsync(id));
        }

        // POST api/<TasksController>
        [HttpPost]
        public async Task<ActionResult<TaskDTO>> Post([FromBody] NewTaskDTO newTaskDTO)
        {
            var task = await _service.CreateAsync(newTaskDTO);
            return CreatedAtAction(nameof(Get), new { id = task.Id }, task);
        }

        // PUT api/<TasksController>
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] UpdateTaskDTO updateTaskDTO)
        {
            await _service.UpdateAsync(updateTaskDTO);
            return Ok();
        }

        // DELETE api/<TasksController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _service.DeleteAsync(id);
            return NoContent();
        }
    }
}
