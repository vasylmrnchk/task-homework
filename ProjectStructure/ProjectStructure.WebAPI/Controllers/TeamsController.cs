﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO.Team;
using ProjectStructure.BLL.Services.Abstract;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class TeamsController : ControllerBase
    {
        readonly ITeamsService _service;
        public TeamsController(ITeamsService service)
        {
            _service = service;
        }

        // GET: api/<TeamsController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamDTO>>> Get()
        {
            return Ok(await _service.ReadAllAsync());
        }

        // GET api/<TeamsController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDTO>> Get(int id)
        {
            return Ok(await _service.ReadAsync(id));
        }

        // POST api/<TeamsController>
        [HttpPost]
        public async Task<ActionResult<TeamDTO>> Post([FromBody] NewTeamDTO newTeamDTO)
        {
            var team = await _service.CreateAsync(newTeamDTO);
            return CreatedAtAction(nameof(Get), new { id = team.Id }, team);
        }

        // PUT api/<TeamsController>
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] UpdateTeamDTO updateTeamDTO)
        {
            await _service.UpdateAsync(updateTeamDTO);
            return Ok();
        }

        // DELETE api/<TeamsController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _service.DeleteAsync(id);
            return NoContent();
        }
    }
}
