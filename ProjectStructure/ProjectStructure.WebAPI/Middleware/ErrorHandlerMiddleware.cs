﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Middleware
{
    /// <summary>
    /// It's global Error handler, that send erro response messages when in controllers throws exception
    /// </summary>
    public class ErrorHandlerMiddleware
    {
        private readonly RequestDelegate _next;

        public ErrorHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception error)
            {
                var response = context.Response;
                response.ContentType = "application/json";
                response.StatusCode = error switch
                {
                    ArgumentException => (int)HttpStatusCode.BadRequest, // request body, or parameters invalid 
                    KeyNotFoundException => (int)HttpStatusCode.NotFound,// resource not found 
                    _ => (int)HttpStatusCode.InternalServerError,// unhandled error
                };
                string message = error?.Message;
                var result = JsonSerializer.Serialize<string>(message);
                await response.WriteAsync(result);
            }
        }
    }
}