﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Repositories.Abstract;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.DAL.Repositories
{
    public abstract class BasicRepository<T> : IBasicRepository<T> where T : Entities.Entity
    {
        protected readonly ProjectStructureDbContext _context;
        public BasicRepository(ProjectStructureDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<T>> ReadAllAsync()
        {
            return await _context.Set<T>().ToListAsync();
        }

        public async virtual Task<T> CreateAsync(T entity)
        {
            await _context.Set<T>().AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<T> ReadAsync(int id)
        {
            return await _context.Set<T>().AsNoTracking()
                                    .Where(item => item.Id == id)
                                    .FirstOrDefaultAsync() ?? throw new KeyNotFoundException($"{typeof(T).Name} not found.");
        }

        public virtual async Task UpdateAsync(T entity)
        {
            var edited = await ReadAsync(entity.Id);
            entity.CreatedAt = edited.CreatedAt;
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var entity = await ReadAsync(id);
            _context.Set<T>().Remove(entity);
            await _context.SaveChangesAsync();
        }
    }
}
