﻿using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Repositories.Abstract;
using System;
using System.Threading.Tasks;

namespace ProjectStructure.DAL.Repositories
{
    public class UsersRepository : BasicRepository<Entities.User>, IUsersRepository
    {
        public UsersRepository(ProjectStructureDbContext context) : base(context)
        {

        }

        public override async Task<Entities.User> CreateAsync(Entities.User entity)
        {
            await CheckTeam(entity.TeamId);
            return await base.CreateAsync(entity);
        }

        public override async Task UpdateAsync(Entities.User entity)
        {
            await CheckTeam(entity.TeamId);
            await base.UpdateAsync(entity);
        }

        private async Task CheckTeam(int? id)
        {
            if (await _context.Set<Entities.Team>().FindAsync(id.GetValueOrDefault()) is null && id.HasValue)
                throw new ArgumentException("Invalid TeamId");
        }
    }
}
