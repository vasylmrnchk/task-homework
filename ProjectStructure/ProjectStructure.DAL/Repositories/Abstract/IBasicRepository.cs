﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.DAL.Repositories.Abstract
{
    public interface IBasicRepository<T> where T : Entities.Entity
    {
        Task<IEnumerable<T>> ReadAllAsync();
        Task<T> ReadAsync(int id);
        Task<T> CreateAsync(T entity);
        Task UpdateAsync(T entity);
        Task DeleteAsync(int id);
    }
}
