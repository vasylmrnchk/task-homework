﻿using ProjectStructure.DAL.Entities;

namespace ProjectStructure.DAL.Repositories.Abstract
{
    public interface IProjectsRepository : IBasicRepository<Project>
    {

    }
}
