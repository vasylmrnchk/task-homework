﻿using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Repositories.Abstract;
using System;
using System.Threading.Tasks;

namespace ProjectStructure.DAL.Repositories
{
    public class ProjectsRepository : BasicRepository<Entities.Project>, IProjectsRepository
    {
        public ProjectsRepository(ProjectStructureDbContext context) : base(context)
        {

        }

        public override async Task<Entities.Project> CreateAsync(Entities.Project entity)
        {
            await CheckAuthorAndTeam(entity);
            return await base.CreateAsync(entity);
        }

        public override async Task UpdateAsync(Entities.Project entity)
        {
            await CheckAuthorAndTeam(entity);
            await base.UpdateAsync(entity);
        }

        private async Task CheckAuthorAndTeam(Entities.Project entity)
        {
            if (await _context.Set<Entities.Team>().FindAsync(entity.TeamId) is null ||
                await _context.Set<Entities.User>().FindAsync(entity.AuthorId) is null)
                throw new ArgumentException("Invalid AuthorId or TeamId");
        }
    }
}
