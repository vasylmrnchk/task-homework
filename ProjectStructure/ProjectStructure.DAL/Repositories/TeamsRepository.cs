﻿using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Repositories.Abstract;

namespace ProjectStructure.DAL.Repositories
{
    public class TeamsRepository : BasicRepository<Team>, ITeamsRepository
    {
        public TeamsRepository(ProjectStructureDbContext context) : base(context)
        {
        }
    }
}
