﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace ProjectStructure.DAL.Migrations
{
    public partial class AddSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 1, new DateTime(2022, 1, 23, 15, 1, 18, 21, DateTimeKind.Local).AddTicks(1931), "Gang of Four" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 2, new DateTime(2022, 1, 23, 15, 1, 18, 30, DateTimeKind.Local).AddTicks(2480), "Justice League" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 3, new DateTime(2022, 1, 23, 15, 1, 18, 30, DateTimeKind.Local).AddTicks(2677), "Reservoir Dogs" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[,]
                {
                    { 5, new DateTime(1967, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), new DateTime(2022, 1, 23, 15, 1, 18, 31, DateTimeKind.Local).AddTicks(912), "gamma@example.com", "Erich", "Gamma", 1 },
                    { 6, new DateTime(1955, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), new DateTime(2022, 1, 23, 15, 1, 18, 31, DateTimeKind.Local).AddTicks(949), "helm@example.com", "Richard", "Helm", 1 },
                    { 7, new DateTime(1945, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), new DateTime(2022, 1, 23, 15, 1, 18, 31, DateTimeKind.Local).AddTicks(971), "johson@example.com", "Ralph", "Johnson", 1 },
                    { 8, new DateTime(1965, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), new DateTime(2022, 1, 23, 15, 1, 18, 31, DateTimeKind.Local).AddTicks(993), "Vlissides@example.com", "John", "Vlissides", 1 },
                    { 9, new DateTime(1929, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), new DateTime(2022, 1, 23, 15, 1, 18, 31, DateTimeKind.Local).AddTicks(1015), "batman@example.com", "Bruce", "Batman", 2 },
                    { 10, new DateTime(1918, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), new DateTime(2022, 1, 23, 15, 1, 18, 31, DateTimeKind.Local).AddTicks(1039), "diana@example.com", "Diana", "Wonder Woman", 2 },
                    { 1, new DateTime(1963, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), new DateTime(2022, 1, 23, 15, 1, 18, 30, DateTimeKind.Local).AddTicks(4673), "mrwhite@example.com", "Larry", "Mr. White", 3 },
                    { 2, new DateTime(1964, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), new DateTime(2022, 1, 23, 15, 1, 18, 31, DateTimeKind.Local).AddTicks(763), "mrorange@example.com", "Freddy", "Mr. Orange", 3 },
                    { 3, new DateTime(1955, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), new DateTime(2022, 1, 23, 15, 1, 18, 31, DateTimeKind.Local).AddTicks(863), "mrblonde@example.com", "Vic", "Mr. Blonde", 3 },
                    { 4, new DateTime(1971, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), new DateTime(2022, 1, 23, 15, 1, 18, 31, DateTimeKind.Local).AddTicks(889), "mrpink@example.com", "Steve", "Mr. Pink", 3 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 2, 5, new DateTime(2022, 1, 23, 15, 1, 18, 31, DateTimeKind.Local).AddTicks(8506), new DateTime(1994, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), "Blonde meets with the Cabots, having completed a four-year jail sentence. To reward him for concealing Joe's name from the", "GoF book", 1 });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 3, 9, new DateTime(2022, 1, 23, 15, 1, 18, 31, DateTimeKind.Local).AddTicks(8599), new DateTime(2014, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), "Superman has become a controversial figure. Bruce is now a billionaire who has operated in Gotham City as the vigilante Batman for twenty ", "Batman vs Supermanan", 2 });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, 1, new DateTime(2022, 1, 23, 15, 1, 18, 31, DateTimeKind.Local).AddTicks(2964), new DateTime(1992, 3, 9, 16, 5, 7, 123, DateTimeKind.Unspecified), "Blonde meets with the Cabots, having completed a four.", "A diamond heist", 3 });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 3, new DateTime(2022, 1, 23, 15, 1, 18, 32, DateTimeKind.Local).AddTicks(4787), "The decorator design pattern is one of the twenty-three well-known GoF design patterns; these describe how to solve recurring design problems and design flexible and reusable object-oriented software—that is, objects which are easier to implement, change, test, and reuse.", null, "Decorator", 5, 2, 0 },
                    { 4, new DateTime(2022, 1, 23, 15, 1, 18, 32, DateTimeKind.Local).AddTicks(4811), "Bruce attends a gala at LexCorp to steal encrypted data from the company's mainframe, but antiquities dealer Diana Prince takes it from him; she returns it after failing to access the information.", null, "Steal encrypted data from the LexCorp", 9, 3, 0 },
                    { 5, new DateTime(2022, 1, 23, 15, 1, 18, 32, DateTimeKind.Local).AddTicks(4833), "Diana arrives, joining Batman and Superman in their fight against the creature.", null, "Diana's aid", 10, 3, 0 },
                    { 1, new DateTime(2022, 1, 23, 15, 1, 18, 32, DateTimeKind.Local).AddTicks(198), "Blonde meets with the Cabots, having completed a four-year jail sentence. To reward him for concealing Joe's name from the authorities, they offer him a no-show job. Blonde insists on returning to real work, and they hire him for the heist.", null, "Blonde returning to work", 3, 1, 0 },
                    { 2, new DateTime(2022, 1, 23, 15, 1, 18, 32, DateTimeKind.Local).AddTicks(4703), " Pink, believes that the job was a setup, and that the police were waiting for them.", null, "Hide the diamonds", 4, 1, 0 },
                    { 6, new DateTime(2022, 1, 23, 15, 1, 18, 32, DateTimeKind.Local).AddTicks(4871), "Orange fabricates an elaborate tale of drug delivery to gain acceptance into the gang", null, "Work undercover in a gang", 2, 1, 0 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3);
        }
    }
}
