﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.DAL.Context
{
    public class ProjectStructureDbContext : DbContext
    {
        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }

        public ProjectStructureDbContext(DbContextOptions<ProjectStructureDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Configure();
            modelBuilder.Seed();

            base.OnModelCreating(modelBuilder);
        }
    }
}
