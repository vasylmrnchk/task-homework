﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;

namespace ProjectStructure.DAL.Context
{
    public static class ModelBuilderExtensions
    {
        public static void Configure(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>()
                .HasOne(p => p.Author)
                .WithMany()
                .HasForeignKey(p => p.AuthorId)
                .OnDelete(DeleteBehavior.NoAction);
            
            modelBuilder.Entity<Project>()
                .HasOne(p => p.Team)
                .WithMany()
                .HasForeignKey(p => p.TeamId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Task>()
                .HasOne(t => t.Project)
                .WithMany(p => p.Tasks)
                .HasForeignKey( t => t.ProjectId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Task>()
                .HasOne(t => t.Performer)
                .WithMany()
                .HasForeignKey(t => t.PerformerId)
                .OnDelete(DeleteBehavior.Cascade);
            
            modelBuilder.Entity<User>()
                .HasOne(u => u.Team)
                .WithMany(t => t.Users)
                .HasForeignKey(u => u.TeamId)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Team>()
                        .Property(t => t.Name)
                        .HasMaxLength(100);

            modelBuilder.Entity<Project>()
                        .Property(p => p.Name)
                        .HasMaxLength(100);

            modelBuilder.Entity<Project>()
                        .Property(p => p.Description)
                        .HasMaxLength(300);

            modelBuilder.Entity<Project>()
                        .Property(p => p.Deadline)
                        .IsRequired();
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            int index = 1;
            var teams = new List<Team>
            {
                new Team { Id = index++, Name = "Gang of Four"},
                new Team { Id = index++, Name = "Justice League"},
                new Team { Id = index++, Name = "Reservoir Dogs"}
            };

            index = 1;
            var users = new List<User>
            {
                new User { Id = index++, LastName = "Mr. White", FirstName= "Larry", Email="mrwhite@example.com", TeamId= 3, BirthDay= new DateTime(1963, 3, 9, 16, 5, 7, 123)},
                new User { Id = index++, LastName = "Mr. Orange", FirstName= "Freddy", Email="mrorange@example.com", TeamId= 3, BirthDay= new DateTime(1964, 3, 9, 16, 5, 7, 123) },
                new User { Id = index++, LastName = "Mr. Blonde", FirstName= "Vic", Email="mrblonde@example.com", TeamId= 3, BirthDay= new DateTime(1955, 3, 9, 16, 5, 7, 123)},
                new User { Id = index++, LastName = "Mr. Pink", FirstName= "Steve", Email="mrpink@example.com", TeamId= 3, BirthDay= new DateTime(1971, 3, 9, 16, 5, 7, 123)},
                new User { Id = index++, LastName = "Gamma", FirstName= "Erich", Email="gamma@example.com", TeamId= 1, BirthDay= new DateTime(1967, 3, 9, 16, 5, 7, 123)},
                new User { Id = index++, LastName = "Helm", FirstName= "Richard", Email="helm@example.com", TeamId= 1, BirthDay= new DateTime(1955, 3, 9, 16, 5, 7, 123)},
                new User { Id = index++, LastName = "Johnson", FirstName= "Ralph", Email="johson@example.com", TeamId= 1, BirthDay= new DateTime(1945, 3, 9, 16, 5, 7, 123) },
                new User { Id = index++, LastName = "Vlissides", FirstName= "John", Email="Vlissides@example.com", TeamId= 1, BirthDay= new DateTime(1965, 3, 9, 16, 5, 7, 123) },
                new User { Id = index++, LastName = "Batman", FirstName= "Bruce", Email="batman@example.com", TeamId= 2, BirthDay= new DateTime(1929, 3, 9, 16, 5, 7, 123)},
                new User { Id = index++, LastName = "Wonder Woman", FirstName= "Diana", Email="diana@example.com", TeamId= 2, BirthDay= new DateTime(1918, 3, 9, 16, 5, 7, 123) }
            };

            index = 1;
            var projects = new List<Project>
            {
                new Project {Id = index++, AuthorId = 1, TeamId = 3, Name = "A diamond heist", Deadline= new DateTime(1992, 3, 9, 16, 5, 7, 123), Description = "Blonde meets with the Cabots, having completed a four."},
                new Project {Id = index++, AuthorId = 5, TeamId = 1, Name = "GoF book", Deadline= new DateTime(1994, 3, 9, 16, 5, 7, 123), Description = "Blonde meets with the Cabots, having completed a four-year jail sentence. To reward him for concealing Joe's name from the"},
                new Project {Id = index++, AuthorId = 9, TeamId = 2, Name = "Batman vs Supermanan", Deadline= new DateTime(2014, 3, 9, 16, 5, 7, 123), Description= "Superman has become a controversial figure. Bruce is now a billionaire who has operated in Gotham City as the vigilante Batman for twenty "}
            };

            index = 1;
            var tasks = new List<Task>
            {
                new Task {Id = index++, ProjectId= 1, PerformerId=3, Name = "Blonde returning to work", Description="Blonde meets with the Cabots, having completed a four-year jail sentence. To reward him for concealing Joe's name from the authorities, they offer him a no-show job. Blonde insists on returning to real work, and they hire him for the heist."},
                new Task {Id = index++, ProjectId= 1, PerformerId=4, Name = "Hide the diamonds", Description=" Pink, believes that the job was a setup, and that the police were waiting for them."},
                new Task {Id = index++, ProjectId= 2, PerformerId=5, Name = "Decorator", Description="The decorator design pattern is one of the twenty-three well-known GoF design patterns; these describe how to solve recurring design problems and design flexible and reusable object-oriented software—that is, objects which are easier to implement, change, test, and reuse."},
                new Task {Id = index++, ProjectId= 3, PerformerId=9, Name = "Steal encrypted data from the LexCorp", Description="Bruce attends a gala at LexCorp to steal encrypted data from the company's mainframe, but antiquities dealer Diana Prince takes it from him; she returns it after failing to access the information."},
                new Task {Id = index++, ProjectId= 3, PerformerId=10, Name = "Diana's aid", Description="Diana arrives, joining Batman and Superman in their fight against the creature."},
                new Task {Id = index++, ProjectId= 1, PerformerId=2, Name = "Work undercover in a gang", Description="Orange fabricates an elaborate tale of drug delivery to gain acceptance into the gang"},
            };

            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);
        }
    }
}
