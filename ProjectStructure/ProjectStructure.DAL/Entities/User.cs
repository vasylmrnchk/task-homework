﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectStructure.DAL.Entities
{
    public class User : Entity
    {
        public Team Team { get; set; }
        public int? TeamId { get; set; }

        [StringLength(26, MinimumLength = 2, ErrorMessage = "Name length must be 2 to 26 characters")]
        public string FirstName { get; set; }

        [StringLength(30, MinimumLength = 2, ErrorMessage = "Surname length must be 2 to 30 characters ")]
        public string LastName { get; set; }

        [EmailAddress(ErrorMessage = "Invalid email address")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime BirthDay { get; set; }
    }
}
