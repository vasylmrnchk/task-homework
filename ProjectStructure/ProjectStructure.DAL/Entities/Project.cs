﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectStructure.DAL.Entities
{
    public class Project : Entity
    {       
        [ForeignKey("AuthorId")]
        public User Author { get; set; }
        public int AuthorId { get; set; }

        public int TeamId { get; set; }
        public Team Team { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }

        public List<Task> Tasks { get; set; }
    }
}
