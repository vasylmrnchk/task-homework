﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectStructure.DAL.Entities
{
    public class Task : Entity
    {
        public Project Project { get; set; }
        public int ProjectId { get; set; }

        [ForeignKey("PerformerId")]
        public User Performer { get; set; }
        public int PerformerId { get; set; }

        [StringLength(100, MinimumLength = 3, ErrorMessage = "Name length must be 3 to 100 characters")]
        public string Name { get; set; }

        [DataType(DataType.MultilineText)]
        [MaxLength(300)]
        public string Description { get; set; }

        [EnumDataType(typeof(TaskState))]
        public TaskState State { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
