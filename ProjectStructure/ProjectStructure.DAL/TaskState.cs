﻿namespace ProjectStructure.DAL
{
    public enum TaskState
    {
        Created,
        Started,
        Completed,
        Canceled
    }
}
