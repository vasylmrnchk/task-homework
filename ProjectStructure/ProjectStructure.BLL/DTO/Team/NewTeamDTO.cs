﻿namespace ProjectStructure.BLL.DTO.Team
{
    public class NewTeamDTO
    {
        public string Name { get; set; }
    }
}
