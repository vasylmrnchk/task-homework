﻿using AutoMapper;
using ProjectStructure.BLL.DTO.Task;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDTO>();
            CreateMap<NewTaskDTO, Task>();
            CreateMap<UpdateTaskDTO, Task>();
        }
    }
}
