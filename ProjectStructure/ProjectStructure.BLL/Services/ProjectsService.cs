﻿using AutoMapper;
using ProjectStructure.BLL.DTO.Project;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Repositories.Abstract;

namespace ProjectStructure.BLL.Services
{
    public class ProjectsService : BaseService<Project, ProjectDTO, NewProjectDTO, UpdateProjectDTO>, IProjectsService
    {
        public ProjectsService(IProjectsRepository repository, IMapper mapper) : base(repository, mapper)
        {

        }
    }
}
