﻿using ProjectStructure.BLL.DTO.User;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.Services.Abstract
{
    public interface IUsersService : IBaseService<User, UserDTO, NewUserDTO, UpdateUserDTO>
    {
    }
}
