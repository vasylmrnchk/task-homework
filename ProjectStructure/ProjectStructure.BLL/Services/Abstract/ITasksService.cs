﻿using ProjectStructure.BLL.DTO.Task;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.Services.Abstract
{
    public interface ITasksService : IBaseService<Task, TaskDTO, NewTaskDTO, UpdateTaskDTO>
    {
    }
}
