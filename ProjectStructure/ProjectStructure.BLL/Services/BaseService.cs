﻿using AutoMapper;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.DAL.Repositories.Abstract;
using System.Collections.Generic;
using System.Threading.Tasks;
using Entity = ProjectStructure.DAL.Entities.Entity;

namespace ProjectStructure.BLL.Services
{
    public class BaseService<T, TDto, TNewDto, TUpdateDto> : IBaseService<T, TDto, TNewDto, TUpdateDto> where T : Entity
    {
        protected readonly IBasicRepository<T> _repository;
        protected readonly IMapper _mapper;
        public BaseService(IBasicRepository<T> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TDto>> ReadAllAsync()
        {
            var entities = await _repository.ReadAllAsync();
            return _mapper.Map<IEnumerable<T>, IEnumerable<TDto>>(entities);
        }

        public async Task<TDto> CreateAsync(TNewDto entityDto)
        {
            var entity = _mapper.Map<T>(entityDto);
            var created = await _repository.CreateAsync(entity);
            return _mapper.Map<TDto>(created);
        }

        public async Task<TDto> ReadAsync(int id)
        {
            var entity = await _repository.ReadAsync(id);
            return _mapper.Map<TDto>(entity);
        }

        public async Task UpdateAsync(TUpdateDto entityDto)
        {
            var entity = _mapper.Map<T>(entityDto);
            await _repository.UpdateAsync(entity);
        }

        public async Task DeleteAsync(int id)
        {
            await _repository.DeleteAsync(id);
        }
    }
}
